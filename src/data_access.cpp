#include "slam/data_access.h"

#include <string.h>

#include <fstream>
#include <iomanip>
#include <iostream>

namespace data_access {

DataAccess::DataAccess(const int Hz) { g_frequency = Hz; }

DataAccess::DataAccess() { ; }

void DataAccess::GetDataFromVBUS() {
  // TODO: read IMU data from CAN BUS
}

void DataAccess::CalculateAverage(const float value, const int count,
                                  float &average) {
  float ratio = (count - 1) / count;
  average = average * ratio + value * (1 - ratio);
}

void DataAccess::GetDataFromYOLO() {
  // TODO: read YOLO data
}

float DataAccess::CalculateTime(const float min, const float second,
                                const float minisecond) {
  return min * 60 + second + minisecond * 0.001;
}

float DataAccess::CalculateInterpolationRatio(const float begin,
                                              const float target,
                                              const float end) {
  float diff_from_end_to_begin = end - begin;
  float diff_from_target_to_begin = target - begin;
  float ratio = diff_from_target_to_begin / diff_from_end_to_begin;
  return ratio;
}

void DataAccess::CalculateInterpolationValue(const std::vector<float> &log_data,
                                             std::vector<float> &sample_data,
                                             const float ratio,
                                             const int index) {
  float begin_value = log_data[index - 1];
  float end_value = log_data[index];

  float interpolation_value = begin_value + (end_value - begin_value) * ratio;
  sample_data.push_back(interpolation_value);
}

}  // namespace data_access
