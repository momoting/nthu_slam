#include "slam/kalman_filter.h"

#include <math.h>

#include <iostream>

namespace kalman_filter {

KalmanFilter::KalmanFilter() { std::cout << "kalman filter init" << std::endl; }

KalmanFilter::~KalmanFilter() {}

void KalmanFilter::Init(Eigen::VectorXf &x_init, Eigen::MatrixXf &P_init,
                        Eigen::MatrixXf &F_init, Eigen::MatrixXf &H_init,
                        Eigen::MatrixXf &R_init, Eigen::MatrixXf &Q_init) {
  x_ = x_init;
  P_ = P_init;
  F_ = F_init;
  H_ = H_init;
  R_ = R_init;
  Q_ = Q_init;
}

void KalmanFilter::Predict() {
  x_ = F_ * x_;
  Eigen::MatrixXf Ft = F_.transpose();
  P_ = F_ * P_ * Ft + Q_;
}

void KalmanFilter::Update(const Eigen::VectorXf &z) {
  Eigen::VectorXf z_predict = H_ * x_;
  Eigen::VectorXf y = z - z_predict;
  Eigen::MatrixXf Ht = H_.transpose();
  Eigen::MatrixXf PHt = P_ * Ht;
  Eigen::MatrixXf S = H_ * PHt + R_;
  Eigen::MatrixXf Si = S.inverse();
  Eigen::MatrixXf K = PHt * Si;

  /* new estimate */
  x_ = x_ + (K * y);
  long x_size = x_.size();
  Eigen::MatrixXf I = Eigen::MatrixXf::Identity(x_size, x_size);
  P_ = (I - K * H_) * P_;
}

}  // namespace kalman_filter
