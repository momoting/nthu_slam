#include "slam/slam.h"

#include <iostream>

#include "slam/ekf.h"
#include "slam/kalman_filter.h"

namespace slam {

Slam::Slam(const int ekf_predict_Hz) {
  std::cout << "slam init" << std::endl;
  fusion_ekf_ = std::make_unique<kalman_filter::FusionEKF>(ekf_predict_Hz);
}

void Slam::Process() {
  std::cout << "slam processing" << std::endl;
  fusion_ekf_
      ->ProcessPrediction();  // TODO: execute it under specific frequency
}

}  // namespace slam

int main(int argc, char **argv) {
  int ekf_predict_Hz = 100;
  std::cout << "-----start slam-----" << std::endl;
  slam::Slam nthu_slam(ekf_predict_Hz);
  nthu_slam.Process();

  return 0;
}
