#include "slam/log_data.h"

#include <string.h>

#include <fstream>
#include <iomanip>
#include <iostream>

namespace data_access {
LogData::LogData() { ; }

LogData::LogData(const int frequency) : DataAccess(frequency) {
  if (SampleData(frequency))
    std::cout << "Log data success" << std::endl;
  else
    std::cout << "Log data fail" << std::endl;
}

void LogData::GetDataFromVBUS() {
  MockGetDataFromVBUS();
  CalculateAverage(imu_data_.yaw, imu_data_.count, imu_data_.yaw_average);
}

void LogData::GetDataFromYOLO() { MockGetDataFromYOLO(); }

void LogData::MockGetDataFromVBUS() {
  int process_num = imu_sample_data_.process_time / imu_sample_data_.period;
  if (process_num == 0)
    float Ts = 0.0;
  else
    float Ts = imu_sample_data_.period;
  imu_data_.ax = imu_sample_data_.ax[process_num];
  imu_data_.ay = imu_sample_data_.ay[process_num];
  imu_data_.wz = imu_sample_data_.wz[process_num];
  imu_data_.yaw = imu_sample_data_.yaw[process_num];
  imu_data_.time = imu_sample_data_.time[process_num];
  imu_data_.count++;
}

void LogData::MockGetDataFromYOLO() {
  // TODO: read log txt file
  yolo_data_.distance = 1.0;
  yolo_data_.direction = 1.0;
  yolo_data_.color = 'y';
  yolo_data_.receive = true;
}

bool LogData::ReadIMULog(std::string filename, int start_line, int end_line) {
  // FIXME: more general. Add two more arguments. goal log variable and data
  // structure
  /*
  delVx = ax*0.01
  delVy = ay*0.01
  gyrZ = wz
  Yaw = P
  */

  /*
  C_length      m       YOLO sense data
  C_dir         rad
  C_color       -       B1/Y2
  CDTDRDY_ind   -       Indicate end of certain frame data
  */
  // goal log variable:
  // struct GoalLogVariable {
  //   float cone_distance
  //   float cone_angle
  // }
  // // data structure:
  // struct LogData {
  //   float yolo_distance
  //   float yolo_angle
  // }
  std::ifstream fs("/home/ldsc/driverless_ws/src/nthu_slam/logdata/" +
                   filename);

  if (!fs.is_open()) {
    std::cout << "Error in open log file" << std::endl;
    return false;
  } else {
    std::cout << "Read log file" << std::endl;
  }

  char buff[256];
  int index = 1;
  bool first_run = true;
  char* p;

  while (!fs.eof()) {
    if (index > end_line) {
      std::cout << "Read log file done" << std::endl;
      break;
    }
    fs.getline(buff, 256);

    if (index >= start_line && index <= end_line) {
      p = strtok(buff, ";");
      float min_int = std::stod(p);

      p = strtok(NULL, ";");
      float sec_int = std::stod(p);

      p = strtok(NULL, ";");
      float mill_sec_int = std::stod(p);

      p = strtok(NULL, ";");
      float value_int = std::stod(p);

      p = strtok(NULL, ";");
      std::string name_str = p;

      if (first_run) {
        imu_log_data_.start_time =
            CalculateTime(min_int, sec_int, mill_sec_int);
        first_run = false;
      }
      if (name_str == "delVx") {
        imu_log_data_.ax.push_back(value_int * 100);
        imu_log_data_.time.push_back(
            CalculateTime(min_int, sec_int, mill_sec_int) -
            imu_log_data_.start_time);
      } else if (name_str == "delVy") {
        imu_log_data_.ay.push_back(value_int * 100);
      } else if (name_str == "gyrZ") {
        imu_log_data_.wz.push_back(value_int);
      } else if (name_str == "Yaw") {
        imu_log_data_.yaw.push_back(value_int);
      } else if (name_str == "C_length") {
        yolo_log_data_.distance.push_back(value_int);  // FIXME:
        yolo_log_data_.time.push_back(
            CalculateTime(min_int, sec_int, mill_sec_int) -
            imu_log_data_.start_time);
      } else if (name_str == "C_dir") {
        yolo_log_data_.direction.push_back(value_int);  // FIXME:
      } else if (name_str == "C_color") {
        yolo_log_data_.time.push_back(value_int);  // FIXME:
      }
    }
    index++;
  }
  return true;
}

bool LogData::SampleData(const float Hz) {
  if (!ReadIMULog("0000233_CONVERTED_20201124211050.txt", 18, 1467661))
    return false;

  int log_data_size = imu_log_data_.ax.size();
  float end_time = imu_log_data_.time[log_data_size - 1];
  imu_sample_data_.period = 1 / Hz;
  float process_time = 0.0;
  int last_sample_index = 0;

  for (process_time = 0.0; process_time <= end_time;
       process_time += imu_sample_data_.period) {
    while (process_time >= imu_log_data_.time[last_sample_index])
      last_sample_index++;

    float begin_time = imu_log_data_.time[last_sample_index - 1];
    float end_time = imu_log_data_.time[last_sample_index];
    float ratio =
        CalculateInterpolationRatio(begin_time, process_time, end_time);

    CalculateInterpolationValue(imu_log_data_.ax, imu_sample_data_.ax, ratio,
                                last_sample_index);
    CalculateInterpolationValue(imu_log_data_.ay, imu_sample_data_.ay, ratio,
                                last_sample_index);
    CalculateInterpolationValue(imu_log_data_.wz, imu_sample_data_.wz, ratio,
                                last_sample_index);
    CalculateInterpolationValue(imu_log_data_.yaw, imu_sample_data_.yaw, ratio,
                                last_sample_index);
    imu_sample_data_.time.push_back(process_time);
  }

  std::cout << "The number of sample data: " << imu_sample_data_.time.size()
            << std::endl;
  return true;
}

}  // namespace data_access
