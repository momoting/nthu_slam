#include "slam/timer.h"

#include <iostream>

EKFtimer::EKFtimer() : g_stop_signal(false) {
  std::cout << "ekf timer created" << std::endl;
  std::thread imu_thread(&EKFtimer::imu_task, this);
  std::thread yolo_thread(&EKFtimer::yolo_task, this);
  imu_thread.detach();
  yolo_thread.detach();
}

EKFtimer::~EKFtimer() {
  g_stop_signal = true;
  imu_thread.join();
  yolo_thread.join();
}

void EKFtimer::start() {
  std::cout << "thread start" << std::endl;
  while (!g_stop_signal) {
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  }
}

void EKFtimer::imu_task() {
  long frequency = 2;  // Hz TODO: should move to constructor
  while (!g_stop_signal) {
    auto start = std::chrono::steady_clock::now();
    /* do task */
    std::unique_lock<std::mutex> lock(
        g_ekf_lock);  // TODO: only lock when updating ekf matrix
    std::cout << "imu task" << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    lock.unlock();
    /* do task */
    auto end = std::chrono::steady_clock::now();

    if (end - start < std::chrono::milliseconds(1000 / frequency)) {
      auto diff_milli_sec =
          std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
      auto sleep_time =
          std::chrono::milliseconds(1000 / frequency - diff_milli_sec.count());
      std::this_thread::sleep_for(sleep_time);
    }
  }
}

void EKFtimer::yolo_task() {
  long frequency = 1;  // Hz TODO: should move to constructor
  while (!g_stop_signal) {
    auto start = std::chrono::steady_clock::now();
    /* do task */
    std::unique_lock<std::mutex> lock(
        g_ekf_lock);  // TODO: only lock when updating ekf matrix
    std::cout << "yolo task" << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(376));
    lock.unlock();
    /* do task */
    auto end = std::chrono::steady_clock::now();

    if (end - start < std::chrono::milliseconds(1000 / frequency)) {
      auto diff_milli_sec =
          std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
      auto sleep_time =
          std::chrono::milliseconds(1000 / frequency - diff_milli_sec.count());
      std::this_thread::sleep_for(sleep_time);
    }
  }
}

int main(int argc, char **argv) {
  std::cout << "timer created" << std::endl;
  EKFtimer timer_test;
  timer_test.start();
  return 0;
}
