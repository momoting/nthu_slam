#include "slam/ekf.h"

#include <math.h>

#include <ctime>  // TODO: or use chrono?
#include <iostream>

#include "Eigen/Dense"

namespace kalman_filter {

FusionEKF::FusionEKF(const int predict_Hz)
    : previous_timestamp_(0.0), is_initialized_(false), imu_receive_count_(0) {
  std::cout << "EKF init" << std::endl;

  int predict_frequency = 100;
#ifdef USE_LOG_DATA
  data_access::LogData data_access_(predict_frequency);
#elif USE_VBUS_DATA
  data_access::DataAccess data_access_(predict_frequency);
#endif
  // data_access_.init(predict_Hz);
  InitializeKalmanMatrix();
}

void FusionEKF::GetIMUData() { data_access_.GetDataFromVBUS(); }

void FusionEKF::GetYOLOData() {}

void FusionEKF::InitializeKalmanMatrix() {
  int state_size = 6;
  // state vector u, v, r, X, Y, P
  ekf_.x_ = Eigen::VectorXf(state_size);

  // covariance matrix
  ekf_.P_ = Eigen::MatrixXf(state_size, state_size);
  ekf_.P_ << 0.01, 0, 0, 0, 0, 0, 0, 0.01, 0, 0, 0, 0, 0, 0, 0.01, 0, 0, 0, 0,
      0, 0, 0.01, 0, 0, 0, 0, 0, 0, 0.01, 0, 0, 0, 0, 0, 0, 0.01;
  // state transistion matrix
  ekf_.F_ = Eigen::MatrixXf(state_size, state_size);
  float init_value = 0.0;
  ekf_.F_ << 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, init_value,
      0, 0, 1, 0, init_value, init_value, 0, 0, 0, 1, init_value, 0, 0, 0, 0, 0,
      1;

  // covariance noise
  ekf_.Q_ = Eigen::MatrixXf(state_size, state_size);
  ekf_.Q_ << 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
}

void FusionEKF::UpdateTransitionFunction(const float Ts) {
  previous_timestamp_ = data_access_.imu_data_.time;
  // FIXME: use k or k-1 state in F_? use k.
  float u = ekf_.x_[0];  // TODO: check whether vector starts from [0]
  float v = ekf_.x_[1];
  float r = data_access_.imu_data_.wz;
  float P = ekf_.x_[5];

  ekf_.F_.row(1) << 1, r * Ts, v * Ts, 0, 0, 0;
  ekf_.F_.row(2) << -r * Ts, 1, -u * Ts, 0, 0, 0;
  ekf_.F_.row(4) << cos(P) * Ts, -sin(P) * Ts, 0, 1, 0,
      (-u * sin(P) - v * cos(P)) * Ts;
  ekf_.F_.row(5) << sin(P) * Ts, cos(P) * Ts, 0, 0, 1,
      (u * cos(P) - v * sin(P)) * Ts;
}

void FusionEKF::ProcessPrediction() {
  // TODO: execute it under specific frequency
  if (!is_initialized_) {
    GetIMUData();
    UpdateTransitionFunction(0);
    is_initialized_ = true;
    imu_receive_count_++;
  } else if (!data_access_.yolo_data_
                  .receive) {  // TODO: need a YOLO data trigger to notify it
    GetIMUData();
    float imu_current_time = data_access_.imu_data_.time;
    float Ts = imu_current_time - data_access_.imu_data_.previous_time;
    PredictState(Ts);
    UpdateTransitionFunction(Ts);
    UpdateCovariance();
    data_access_.imu_data_.previous_time = imu_current_time;
    imu_receive_count_++;
  }
}

void FusionEKF::ProcessUpdate() { /* TODO: Update EKF by YOLO data */
}

void FusionEKF::PredictState(const float Ts) {
  // TODO: reference table 2-4 to predict state. not using F matrix
  /*
u_predict = u + (imu_ax + v*r)*Ts
v_predict = v + (imu_ay - u*r)*Ts
r_predict = imu_wz
X_predict = X + (u*cos(P) - v*sin(P))*Ts
Y_predict = Y + (u*sin(P) + v*cos(P))*Ts
P_predict = P + delta imu_p north
*/
  float u = ekf_.x_[0];
  float v = ekf_.x_[1];
  float r = data_access_.imu_data_.wz;
  float imu_ax = data_access_.imu_data_.ax;
  float imu_ay = data_access_.imu_data_.ay;
  float delta_p =
      data_access_.imu_data_.yaw - data_access_.imu_data_.yaw_average;
  float P = ekf_.x_[5];

  ekf_.x_[0] = u + (imu_ax + v * r) * Ts;
  ekf_.x_[1] = v + (imu_ay - u * r) * Ts;
  ekf_.x_[2] = r;
  ekf_.x_[3] += (u * cos(P) - v * sin(P)) * Ts;
  ekf_.x_[4] += (u * sin(P) + v * cos(P)) * Ts;
  ekf_.x_[5] += delta_p;
}

void FusionEKF::UpdateCovariance() {
  Eigen::MatrixXf Ft = ekf_.F_.transpose();
  ekf_.P_ = ekf_.F_ * ekf_.P_ * Ft + ekf_.Q_;
}

}  // namespace kalman_filter
