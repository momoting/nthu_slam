#ifndef EKF_H_
#define EKF_H_

#define USE_LOG_DATA
// #define USE_VBUS_DATA

#include <memory>

#include "Eigen/Dense"
#include "slam/kalman_filter.h"

#ifdef USE_LOG_DATA
#include "slam/log_data.h"
#elif USE_VBUS_DATA
#include "slam/data_access.h"
#endif
namespace kalman_filter {

class FusionEKF {
 public:
  FusionEKF(const int predict_Hz);

  void ProcessPrediction();
  void ProcessUpdate();

  kalman_filter::KalmanFilter ekf_;

#ifdef USE_LOG_DATA
  // // std::unique_ptr<data_access::LogData> data_access_ = nullptr;
  data_access::LogData data_access_;
#elif USE_VBUS_DATA
  // // std::unique_ptr<data_access::DataAccess> data_access_ = nullptr;
  data_access::DataAccess data_access_;
#endif

 protected:
 private:
  void InitializeKalmanMatrix();
  void GetIMUData();
  void GetYOLOData();
  void UpdateTransitionFunction(const float Ts);
  void PredictState(const float Ts);
  void UpdateCovariance();
  double previous_timestamp_;
  int imu_receive_count_;
  bool is_initialized_;
  Eigen::MatrixXf H_jacobian;  // measurement function for
};

}  // namespace kalman_filter

#endif
