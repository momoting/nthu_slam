#ifndef DATA_ACCESS_H_
#define DATA_ACCESS_H_
#include <string>
#include <vector>

namespace data_access {
struct IMUData {
  float ax;
  float ay;
  float wz;
  float yaw;
  float time = 0.0;
  float previous_time = 0.0;
  int count = 0;  // TODO: use long int?
  float yaw_average = 0.0;
};

struct YOLOData {
  float distance;
  float direction;
  char color;
  bool receive = false;
};

class DataAccess {
 public:
  DataAccess(const int Hz);
  DataAccess();
  virtual void GetDataFromVBUS();
  virtual void GetDataFromYOLO();

  IMUData imu_data_;
  YOLOData yolo_data_;

 protected:
  float CalculateTime(const float min, const float second,
                      const float minisecond);
  float CalculateInterpolationRatio(const float begin, const float target,
                                    const float end);
  void CalculateInterpolationValue(const std::vector<float> &log_data,
                                   std::vector<float> &sample_data,
                                   const float ratio, const int index);
  void CalculateAverage(const float value, const int count, float &average);
  int g_frequency;
};

}  // namespace data_access

#endif
