#include <chrono>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>

class EKFtimer {
  bool clear = false;

 public:
  EKFtimer();
  ~EKFtimer();
  void imu_task();
  void yolo_task();
  void start();
  bool g_stop_signal;

 private:
  std::mutex g_ekf_lock;
  // imu thread
  std::thread imu_thread;
  std::recursive_mutex imu_mutex_;
  std::condition_variable_any imu_cond_;

  // yolo thread
  std::thread yolo_thread;
  std::condition_variable_any yolo_cond_;
};
