#ifndef LOG_DATA_H_
#define LOG_DATA_H_

#include <vector>

#include "slam/data_access.h"

namespace data_access {
struct IMULogData {
  std::vector<float> ax;
  std::vector<float> ay;
  std::vector<float> wz;
  std::vector<float> yaw;
  std::vector<float> time;
  float start_time;
};

struct IMUSampleData {
  std::vector<float> ax;
  std::vector<float> ay;
  std::vector<float> wz;
  std::vector<float> p;
  std::vector<float> yaw;
  std::vector<float> time;
  float process_time;
  float period;
};

struct YOLOLogData {
  std::vector<float> distance;
  std::vector<float> direction;
  std::vector<char> color;
  std::vector<float> time;
  bool receive = false;
};

class LogData : public DataAccess {
 public:
  LogData();
  LogData(const int frequency);
  void GetDataFromVBUS() override;
  void GetDataFromYOLO() override;

  IMUData imu_data_;
  YOLOData yolo_data_;

 private:
  bool SampleData(const float Hz);  // TODO: should not appear in ekf.cpp srcipt
  bool ReadIMULog(std::string filename, int start_line, int end_line);
  // bool ReadYOLOLog(std::string filename, int start_line, int end_line);
  void MockGetDataFromVBUS();
  void MockGetDataFromYOLO();
  IMULogData imu_log_data_;
  IMUSampleData imu_sample_data_;
  YOLOLogData yolo_log_data_;
};

}  // namespace data_access

#endif
