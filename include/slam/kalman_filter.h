#ifndef KALMAN_FILTER_H_
#define KALMAN_FILTER_H_
#include "Eigen/Dense"

namespace kalman_filter {

class KalmanFilter {
 public:
  // state vector
  Eigen::VectorXf x_;

  // state covariance matrix
  Eigen::MatrixXf P_;

  // state transistion matrix
  Eigen::MatrixXf F_;

  // process covariance matrix
  Eigen::MatrixXf Q_;

  // measurement martix
  Eigen::MatrixXf H_;

  // measurement covariance matrix
  Eigen::MatrixXf R_;

  /**
   * @brief Constructor
   */
  KalmanFilter();

  /**
   * @brief Destructor
   */
  virtual ~KalmanFilter();

  /**
   * @brief Initialize kalman filter
   * @param x_init Initial state
   * @param P_init Initial state covariance
   * @param F_init Transition matrix
   * @param H_init Measurement matrix
   * @param R_init Measurement covariance matrix
   * @param Q_init Process covariance matrix
   */
  void Init(Eigen::VectorXf &x_init, Eigen::MatrixXf &P_init,
            Eigen::MatrixXf &F_init, Eigen::MatrixXf &H_init,
            Eigen::MatrixXf &R_init, Eigen::MatrixXf &Q_init);

  /**
   * @brief Predict the state and the state covariance
   * @param delta_T Time between k and k+1 state
   */
  void Predict();

  /**
   * @brief Update the state by using standard kalman filter equations
   * @param z The measurement at k+1
   */
  void Update(const Eigen::VectorXf &z);

  /**
   * @brief Update the state by using extended kalman filter equations
   * @param z The measurement at k+1
   */
  void UpdateEKF(const Eigen::VectorXf &z);
};

}  // namespace kalman_filter

#endif
