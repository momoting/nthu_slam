#ifndef SLAM_H_
#define SLAM_H_
#include <memory>

#include "slam/ekf.h"

namespace slam {

class Slam {
 public:
  Slam(const int ekf_predict_Hz);
  void Process();

 protected:
  std::unique_ptr<kalman_filter::FusionEKF> fusion_ekf_ = nullptr;
};

class ParticleFilter {
 public:
 protected:
};

}  // namespace slam

#endif
