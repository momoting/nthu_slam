# Overview and Usage

This algorithm is based on EKF and particle filter.

Apply on NTHU driverless student formula.

## Get started

Goal: estimate car pose and cone location.

Input: data from IMU and cone measurement from YOLO.

Output: car pose and cone location.

## Process

#### Prediction (cold loop)

Each particle has its own EKF.

Continuously predicts car pose by IMU data.

1. get IMU data
2. define state

    a. state: u, v, r, X, Y, P

        u: velocity u. imu x-axis(forward direction)
        v: velocity r. imu y-axis(leftward direction)
        r: velocity v. imu z-axis(upward direction)
        X: car pose x in map coordinate
        Y: car pose y in map coordinate
        P: angle between car and map x-axis

    ```cpp
    state vector(6x1) x << u,
                           v,
                           r,
                           X,
                           Y,
                           P
    ```

    b. state predict function:

        u+ = u + (imu_ax + v*r)*Ts
        v+ = v + (imu_ay - u*r)*Ts
        r+ = imu_wz
        X+ = X + (u*cos(P) - v*sin(P))*Ts
        Y+ = Y + (u*sin(P) + v*cos(P))*Ts
        P+ = P + delta imu_p north

    ```cpp
    state predict vector(6x1) xp << u+,
                                    v+,
                                    r+,
                                    X+,
                                    Y+,
                                    P+
    ```

3. calculate transition matrix "F" from state predict model

    ```cpp
    F <<      1,         r*Ts,     v*Ts, 0, 0,            0,
            -r*Ts,         1,     -u*Ts, 0, 0,            0,
              0,           0,        0,  0, 0,            0,
          cos(P)*Ts,  -sin(P)*Ts,    0,  1, 0, [-u*sin(P)-v*cos(P)]*Ts,
          sin(P)*Ts,   cos(P)*Ts,    0,  0, 1,  [u*cos(P)-v*sin(P)]*Ts,
              0,           0,        0,  0, 0,            1
    ```

4. make prediction
    ```
    xp = F * x
    Σp = F * Σ * FT + Q(control noise)
    ```
        x: current state
        F: transition matrix
        xp: predict state
        Σ: current covariance
        Σp: predict covariance

5. Continously making prediction

    Before receive measurement from hot loop, repeat step 1 to 4.

#### Update (hot loop)

Each particle uses landmark measurement to update its pose.

For the first run, initialize landmarks by measurement.

For the other runs, use Gaussian distribution to analyse upcoming measurement.

If the landmark probability is higher then threshold, create a new landmark in this particle. Otherwise, update car position based on landmark positions.

First run:

1. initialize landmarks

    Create landmark in each particle

Rest of runs:

2. Find landmark association:

    Compare observed landmarks with saved ones. Calculate jacobian (car pose (X, Y, P) to measurement(d, θ)). Obtain landmark adjust covariance. With the covariance, we can obtain Gaussian distribution and the measurement accuracy.

3. Create or update landmark

    If the accuracy from 2. is smaller than thres, create a new landmark(do 1. create new landmark) in particle. Otherwise, update landmark, including its adjust covariance and jacobian.


#### Resample (particle filter)

Compare all landmark accuracy in each particle.

Define highest one as current pose, then spray particles again.
